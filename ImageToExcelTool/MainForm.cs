﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ImageToExcelTool
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private class ImageFileInfo
        {
            //public int 序号 { get; set; }
            public string 图片路径 { get; set; }
            public string 图片名称 { get; set; }
        }

        private List<ImageFileInfo> m_imageinfofilelist = new List<ImageFileInfo>();
        private List<string> m_filenamelist = new List<string>();
        //private int m_index = 1;
        private DataTable m_dt = null;

        private void 打开图片文件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image Files(*.bmp;*.jpg;*.png;*.gif)|*.bmp;*.jpg;*.png;*.gif";
            dlg.Multiselect = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                foreach (string filename in dlg.FileNames)
                {
                    if (m_filenamelist.IndexOf(filename) == -1)
                    {
                        m_filenamelist.Add(filename);
                        m_imageinfofilelist.Add(new ImageFileInfo()
                        {
                            //序号 = m_index++,
                            图片名称 = Path.GetFileName(filename).Replace(Path.GetExtension(filename), ""),
                            图片路径 = filename
                        });
                    }
                }
                RefreshData(m_dt = m_imageinfofilelist.ToDataSet().Tables[0]);
            }
        }

        private void RefreshData(DataTable dt)
        {
            if (dt != null)
            {
                int[] columnwidths = null;
                if (dataGridView1.ColumnCount > 1)
                {
                    columnwidths = new int[dataGridView1.ColumnCount];
                    foreach (DataGridViewColumn col in dataGridView1.Columns)
                        columnwidths[col.Index] = col.Width;
                }
                CommonClass.UpdateGetDataStatus(dataGridView1);
                dataGridView1.DataSource = dt;
                if (columnwidths != null)
                    foreach (DataGridViewColumn col in dataGridView1.Columns)
                        col.Width = columnwidths[col.Index];
                this.toolStripStatusLabel1.Text = "记录数：" + dt.Rows.Count;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (m_dt != null)
            {
                DataTable dt = m_dt.Clone();
                DataRow[] rows = m_dt.Select(dt.Columns[1].ColumnName + " like '%" + textBox1.Text + "%'");
                
                foreach (DataRow row in rows)
                {
                    DataRow newrow = dt.NewRow();
                    for (int i = 0; i < dt.Columns.Count; ++i)
                        newrow[i] = row[i];
                    dt.Rows.Add(newrow);
                }
                RefreshData(dt);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Excel Files(*.xls)|*.xls";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if(File.Exists(dlg.FileName))
                        File.Delete(dlg.FileName);
                    using (ExcelHelper helper = new ExcelHelper(dlg.FileName))
                    {
                        DataTable dt = helper.GetDgvToTable(dataGridView1);
                        helper.DataTableToExcel(dt, new List<int>(new int[] { 0 }));
                    }
                    MessageBox.Show("导出为：" + dlg.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void 关于本程序ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Text + "\r\n\r\n开发者：浩宇软件\r\nE-mail：752923276@qq.com", "关于本系统", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void 检测更新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CommonClass.CheckUpdate(this);
        }

        private void 清除所有数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("是否确定清除所有数据？", "请选择", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.No)
                return;
            m_filenamelist.Clear();
            m_imageinfofilelist.Clear();
            CommonClass.UpdateGetDataStatus(dataGridView1);
            dataGridView1.Columns.Add("Column1", "数据项");
            this.toolStripStatusLabel1.Text = "记录数：0";
            MessageBox.Show("数据已清除");
        }
    }
}
